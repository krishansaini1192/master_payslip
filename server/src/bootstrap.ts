import Taxes from './models/taxes';

const taxesToSave = [
  {
    startAmount : 0,
    endAmount : 18200,
    minimumTax : 0,
    tax : 0
  },
  {
    startAmount : 18201,
    endAmount : 37000,
    minimumTax : 0,
    tax : 19
  },
  {
    startAmount : 37001,
    endAmount : 87000,
    minimumTax : 3572,
    tax : 32.5
  },
  {
    startAmount : 87001,
    endAmount : 180000,
    minimumTax : 19822,
    tax : 37
  },
  {
    startAmount : 180001,
    endAmount : 999999999,
    minimumTax : 54232,
    tax : 45
  }
];

export const addTaxes = async function () {
  try{
    const taxes = await Taxes.find();
    if(taxes && taxes.length > 0){
      return "Taxes already exists in the DB";
    }
    await Taxes.insertMany(taxesToSave);
    return "Taxes Saved.";
  }
  catch(err){
    console.log(err);
    return err;
  }
};
