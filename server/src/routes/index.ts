import { Router } from 'express';
import userRoutes from './user';

const routes = Router();

routes.use('/user', userRoutes);

routes.get('/', (req, res) => {
  res.render('./welcome.pug', {
    name: 'Payslip'
  });
});

export default routes;
