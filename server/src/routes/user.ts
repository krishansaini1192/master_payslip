import { Router } from 'express';
import * as pdf from 'html-pdf';
import * as md5 from 'md5';
import * as Joi from 'joi';
import * as handlebars from 'handlebars';
import { joiFailurePrettier, capitalizeFirstLetter } from '../lib/common_functions';
import Taxes from '../models/taxes';
const userRoutes = Router();

userRoutes.get('/payslip', async (req, res) => {
  try {
    const ret = Joi.validate(
      req.query,
      Joi.object({
        first_name: Joi.string()
          .min(2)
          .required(),
        last_name: Joi.string()
          .min(2)
          .required(),
        annual_salary: Joi.string().required(),
        super_rate: Joi.string().required(),
        payment_start_date: Joi.date().required()
      })
    );
    if (ret.error) {
      throw joiFailurePrettier(ret);
    }

    let incomeTax = 0;
    const getTaxes = await Taxes.findOne({
      startAmount: { $lte: parseInt(req.query.annual_salary) },
      endAmount: { $gte: parseInt(req.query.annual_salary) }
    });
    incomeTax =
      getTaxes.minimumTax +
      (parseInt(req.query.annual_salary) - getTaxes.startAmount + 1) * getTaxes.tax;
    incomeTax = incomeTax / 1200;
    console.log(getTaxes);
    const grossIncome = (parseInt(req.query.annual_salary) / 12).toFixed(0);
    const superannuation = grossIncome * (parseFloat(req.query.super_rate) / 100);
    const fileName = md5(
      req.query.first_name + req.query.last_name + req.query.annual_salary + +new Date()
    );
    const pdfHTML = await getPdfHtml({
      name:
        capitalizeFirstLetter(req.query.first_name) +
        ' ' +
        capitalizeFirstLetter(req.query.last_name),
      payPeriod: 12 - new Date(req.query.payment_start_date).getMonth(),
      grossIncome: grossIncome,
      incomeTax: incomeTax.toFixed(0),
      netIncome: (grossIncome - incomeTax).toFixed(0),
      superannuation: superannuation.toFixed(0)
    });

    const options = { format: 'Letter', orientation: 'landscape' };
    await pdf
      .create(pdfHTML, options)
      .toFile('./uploads/' + fileName + '.pdf', async (err, pdfData) => {
        if (err) throw err;
        res.status(200).json({ statusCode: 200, data: pdfData });
      });
  } catch (err) {
    console.log(err);
    res.status(400).json({ statusCode: 400, error: err });
  }
});

const getPdfHtml = async (data) => {
  try {
    const source =
      '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"></head><body><table style="text-align: left;"><thead><th style="padding: 10px;">Name</th><th style="padding:10px;">Pay Period</th><th style="padding: 10px;">Gross Income</th><th style="padding: 10px;">Income Tax</th><th style="padding: 10px;">Net Income</th><th style="padding: 10px;">Superannuation</th></thead><tbody><tr><td style="padding: 10px;">{{name}}</td><td style="padding: 10px;">{{payPeriod}}</td><td style="padding: 10px;">{{grossIncome}}</td><td style="padding: 10px;">{{incomeTax}}</td><td style="padding: 10px;">{{netIncome}}</td><td style="padding: 10px;">{{superannuation}}</td></tr></tbody></table></body></html>';
    const template = await handlebars.compile(source);
    return await template(data);
  } catch (e) {
    throw e;
  }
};

export default userRoutes;
