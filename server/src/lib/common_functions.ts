export const joiFailurePrettier = (error) => {
  try {
    console.log(error.error.details[0].message);
    let customErrorMessage = '';
    if (error.error.details[0].message.indexOf('[') > -1) {
      customErrorMessage = error.error.details[0].message.substr(
        error.error.details[0].message.indexOf('[')
      );
    } else {
      customErrorMessage = error.error.details[0].message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    return customErrorMessage;
  } catch (err) {
    throw err;
  }
};
export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
