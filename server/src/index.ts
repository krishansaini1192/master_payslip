import * as express from 'express';
import routes from './routes';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { SERVER } from './constants';
import './database_connection/mongo_connect';
import {addTaxes} from './bootstrap';
const app = express();

const port = SERVER.PORT;
app.use(bodyParser.json());
app.use(helmet());
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', routes);
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');
app.listen(port, async (err) => {
  if (err) {
    console.log('Server could not start ', err);
    return;
  }
  console.log('Server running at port : ', port);
  const savedTaxes = await addTaxes();
  console.log(savedTaxes);
});

