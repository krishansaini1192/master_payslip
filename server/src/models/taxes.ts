import { Schema, model } from 'mongoose';

const Taxes = new Schema(
  {
    startAmount : { type: Number, required: true },
    endAmount : { type: Number, required: true },
    minimumTax : { type: Number, required: true },
    tax : { type: Number, required: true } // in cents
  },
  { timestamps: true }
);

export default model('Taxes', Taxes);
