# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This Repository is used to create payslips of employees from their salary details.

### How do I get set up? ###

Must Have:
1. MongoDB, Node, NPM
2. npm install -g typescript ts-node
3. got to server folder, run npm i
4. npm start, this will start the server on port 8888.
5. go to client folder, run npm i
6. npm start, this will open the client on browser, localhost:3000
NOTE: If backend code is placed on cloud, then only PDF will be downloaded else link needs to be opened manually because some browsers does not allow access to download local files.

### Who do I talk to? ###

Krishan Saini