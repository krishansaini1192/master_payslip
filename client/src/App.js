import React, { Component } from "react";
import qs from "querystring";
import { Container, Form, Button, Message } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";

const forceDownload = href => {
  // To download the file.
  const link = document.createElement("a");
  link.href = "file://" + href;
  link.target = "_blank";
  link.click();
};

const initialState = {
  error: "",
  success: "",
  fields: {
    first_name: "",
    last_name: "",
    annual_salary: 0,
    super_rate: 0,
    payment_start_date: ""
  }
};

class App extends Component {
  state = initialState;

  handleValueChange = event => {
    this.setState({
      ...this.state,
      fields: {
        ...this.state.fields,
        [event.target.name]: [event.target.value]
      }
    });
  };

  handleError = msg => {
    console.warn("Got error", msg);

    this.setState({
      ...this.state,
      error: msg
    });
  };

  handleSuccess = res => {
    this.setState({
      ...this.state,
      success: "PDF Created. Copy link and open in browser: "+ res.data.filename
    });

    const pdf = res.data.filename;
    forceDownload(pdf);

    this.setState({
      ...this.state,
      fields: {
        ...initialState.fields
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const url = "http://localhost:8888/user/payslip";
    this.setState({
      ...this.state,
      error: "",
      success: ""
    });

    return fetch(`${url}?${qs.stringify(this.state.fields)}`)
      .then(res => res.json())
      .then(res => {
        if (res.statusCode !== 200) {
          throw res;
        }

        this.handleSuccess(res);
      })
      .catch(err => this.handleError(err.error));
  };

  render() {
    const error = this.state.error;
    const success = this.state.success;

    return (
      <Container error={error}>
        {error && <Message error header="Failed to submit" content={error} />}
        {success && (
          <Message success header="Submission successfull" content={success} />
        )}
        <Form onSubmit={this.handleSubmit}>
          <Form.Input
            label="First Name"
            name="first_name"
            type="text"
            value={this.state.fields.first_name}
            onChange={this.handleValueChange}
          />
          <Form.Input
            label="Last Name"
            name="last_name"
            type="text"
            value={this.state.fields.last_name}
            onChange={this.handleValueChange}
          />
          <Form.Input
            label="Annual Salary"
            name="annual_salary"
            type="number"
            value={this.state.fields.annual_salary}
            onChange={this.handleValueChange}
          />
          <Form.Input
            label="Super Rate"
            name="super_rate"
            type="number"
            min="0"
            max="12"
            value={this.state.fields.super_rate}
            onChange={this.handleValueChange}
          />
          <Form.Input
            label="Start Date"
            name="payment_start_date"
            type="date"
            value={this.state.fields.payment_start_date}
            onChange={this.handleValueChange}
          />

          <Button primary>Submit</Button>
        </Form>
      </Container>
    );
  }
}

export default App;
